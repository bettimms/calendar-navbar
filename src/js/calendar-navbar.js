/**
 * Created by betim on 2/16/2017.
 */
var DomHelper = (function () {
    function isElement(o) {
        return (
            typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
                o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string"
        );
    }

    function hasClass(el, cls) {
        if (el) return el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    }

    function addClass(el, classes) {
        var array = classes.split(' ');
        for (var i = 0, length = array.length; i < length; i++) {
            var cls = array[i];
            if (!hasClass(el, cls) && cls !== "") {
                el.classList.add(cls);
            }
        }
    }

    function removeClass(el, classes) {
        var array = classes.split(' ');
        for (var i = 0, length = array.length; i < length; i++) {
            var cls = array[i];
            if (cls) el.classList.remove(array[i]);
        }
    }

    return {
        isElement: isElement,
        removeClass: removeClass,
        hasClass: hasClass,
        addClass: addClass
    }
})();
var DateUtility = (function () {
    function getNextWeek(week) {
        var date = new Date(week);
        date.setDate(date.getDate() + 7);
        return date;
    }

    function getByDate(date, daysAhead) {
        var d = new Date(date);
        if (daysAhead) d.setDate(d.getDate() + daysAhead);
        return d;
    }

    function getPreviousWeek(week) {
        var date = new Date(week);
        date.setDate(date.getDate() - 7);
        return date;
    }

    function compare(date1, date2) {
        if (isNaN(date1) || isNaN(date2)) {
            throw new Error(date1 + " - " + date2);
        } else if (date1 instanceof Date && date2 instanceof Date) {
            return (date1 < date2) ? -1 : (date1 > date2) ? 1 : 0;
        } else {
            throw new TypeError(date1 + " - " + date2);
        }
    };

    function getWeekNumber(date) {
        var d = new Date(date);
        d.setHours(0, 0, 0, 0);
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
    }

    function nth(n) {
        return ["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th"
    }

    function getByDayName(dayName, d) {
        if (!dayName) return;
        var dayNumber = 0;
        switch (dayName) {
            case "Sunday":
                dayNumber = 0;
                break;
            case "Monday":
                dayNumber = 1;
                break;
            case "Tuesday":
                dayNumber = 2;
                break;
            case "Wednesday":
                dayNumber = 3;
                break;
            case "Thursday":
                dayNumber = 4;
                break;
            case "Friday":
                dayNumber = 5;
                break;
            case "Saturday":
                dayNumber = 6;
                break;
        }

        var userDate = new Date(d || new Date());
        var currentDayNumber = userDate.getDay();
        var difference = userDate.getDate() - currentDayNumber + (currentDayNumber == 0 ? -6 : dayNumber);

        var newDate = new Date(userDate);
        var resultDate = new Date(d ? newDate.setDate(difference) : new Date().setDate(difference));

        if (DateUtility.compare(resultDate, userDate) <= 0) return resultDate;//Smaller or equal to userdate

        var prevWeek = DateUtility.getPreviousWeek(resultDate);
        return prevWeek;
    }

    return {
        getNextWeek: getNextWeek,
        getPreviousWeek: getPreviousWeek,
        compare: compare,
        getWeekNumber: getWeekNumber,
        getByDate: getByDate,
        getByDayName: getByDayName,
        nth: nth
    }
})();

(function () {
    //Define constructor
    //Handler cache
    var eventHandler = {
        fire: function (event) {
            if (typeof event === "function") {
                var args = Array.prototype.slice.call(arguments, 1);
                event.apply(this, args);
            }
        }
    }
    this.CalendarNavbar = function () {
        this.monthNamesShort = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        this.monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        this.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        this.date = new Date();

        this.rightButton = null;
        this.leftButton = null;
        this.todayButton = null;

        var defaults = {
            startDay: "",//If set week will keep keep it first
            referenceToStartDay: false,//If true days will be set based on startDay
            startDate: this.date,
            className: "",
            rightButtonText: "Next",
            leftButtonText: "Previous",
            todayClass: "",
            buttonClass: "",
            onSelect: "",
            onNext: "",
            onPrevious: "",
            onInitComplete: "",
            onToday: "",
            onChange: "",
            locale: "en-us",
            valueFormat: function (a, b) {
                return a;
            }
        }

        //First parameter should be $el or id of an $el
        this.$el = DomHelper.isElement(arguments[0])
            ? arguments[0] : document.getElementById(arguments[0]);

        var options = arguments[1];
        if (options && typeof options === "object") {
            this.options = extend({}, defaults, options);
        }
        else {
            this.options = defaults;
        }
        //Start week on day or today
        this.currentWeek = DateUtility.getByDayName(this.options.startDay) || this.options.startDate;
        init.call(this);
    }

    //#region Public methods
    CalendarNavbar.prototype.setWeek = function (week) {
        //TODO optimize
        if (this.options.referenceToStartDay) {
            var dayName = this.dayNames[week.getDay()];
            var date = DateUtility.getByDayName(this.options.startDay, week);
            var nextWeek = DateUtility.getNextWeek(date);
            setWeek.call(this, new Date(date), nextWeek);
            setCurrentWeek.call(this, date);

            var newCurrentWeek = getDateObj.call(this, date);
            var fullDate = getFullDateObj(newCurrentWeek, this.getByDayNumber(6));
            eventHandler.fire.call(this, this.options.onChange, [], fullDate);
            // console.log(dayName, date);
        } else {
            var currentWeek = new Date(this.currentWeek);//To be changed with date
            var date = new Date(week);
            setCurrentWeek.call(this, date);
            var nextWeek = DateUtility.getNextWeek(date);
            setWeek.call(this, new Date(date), nextWeek);

            var newCurrentWeek = getDateObj.call(this, date),
                prevWeek = getDateObj.call(this, currentWeek);
            var fullDate = getFullDateObj(newCurrentWeek, this.getByDayNumber(6), prevWeek);
            eventHandler.fire.call(this, this.options.onChange, [], fullDate);
        }
    }
    CalendarNavbar.prototype.getWeek = function () {
        return getDateObj.call(this, this.currentWeek);
    }
    CalendarNavbar.prototype.remove = function () {
        this.$el.remove();
    }
    CalendarNavbar.prototype.getDayIndex = function (date) {
        //TODO optimize
        var currentDate = new Date(this.currentWeek);
        currentDate.setHours(0, 0, 0, 0);
        var endDate = DateUtility.getNextWeek(this.currentWeek);
        endDate.setHours(0, 0, 0, 0);
        var dateToCompare = new Date(date);
        dateToCompare.setHours(0, 0, 0, 0);

        for (var d = currentDate, i = 0; d < endDate; d.setDate(d.getDate() + 1), i++) {
            var newDate = new Date(d);
            newDate.setHours(0, 0, 0, 0);
            if (DateUtility.compare(newDate, dateToCompare) == 0) {
                return i;
            }
        }
    }
    //Get a date on current week
    CalendarNavbar.prototype.getByDayNumber = function (dayNumber) {
        if (dayNumber >= 0 && dayNumber < 7) {
            var date = new Date(this.currentWeek);
            date.setDate(date.getDate() + dayNumber);
            var dateObj = getDateObj.call(this, date);
            return dateObj;
        }
    }
    //#endregion

    //#region Private methods
    function init() {
        generateHtml.call(this);
        initEvents.call(this);

        var currentWeek = new Date(this.currentWeek);
        var nextWeek = DateUtility.getNextWeek(currentWeek);
        setWeek.call(this, currentWeek, nextWeek);
    }

    function onSelect(evt) {
        deselectAll();
        var td = evt.target.nodeName === "SPAN" ? evt.target.parentNode : evt.target;
        DomHelper.addClass(td, "active");
        eventHandler.fire.call(this, this.options.onSelect, evt, td.date);
    }

    function onNext(evt) {
        var currentWeek = new Date(this.currentWeek);
        var nextWeek = DateUtility.getNextWeek(currentWeek);
        setCurrentWeek.call(this, nextWeek);
        //Display 1 week ahead
        var weekToDisplay = DateUtility.getNextWeek(nextWeek);

        var newCurrentWeek = getDateObj.call(this, nextWeek),
            prevWeek = getDateObj.call(this, currentWeek);
        setWeek.call(this, nextWeek, weekToDisplay);
        var fullDate = getFullDateObj(newCurrentWeek, this.getByDayNumber(6), prevWeek);
        eventHandler.fire.call(this, this.options.onNext, evt, fullDate);
        eventHandler.fire.call(this, this.options.onChange, evt, fullDate);
    }

    function onPrevious(evt) {
        var currentWeek = new Date(this.currentWeek);
        var prevWeek = DateUtility.getPreviousWeek(currentWeek);
        setCurrentWeek.call(this, prevWeek);

        var previousWeek = getDateObj.call(this, currentWeek),
            newCurrentWeek = getDateObj.call(this, prevWeek);
        setWeek.call(this, prevWeek, currentWeek);
        var fullDate = getFullDateObj(newCurrentWeek, this.getByDayNumber(6), previousWeek);
        eventHandler.fire.call(this, this.options.onPrevious, evt, fullDate);
        eventHandler.fire.call(this, this.options.onChange, evt, fullDate);
    }

    function onToday(evt) {
        var today = new Date(this.date),
            todayObj = getDateObj.call(this, today);

        var nextWeek = DateUtility.getByDate(today, 6),
            nextWeekObj = getDateObj.call(this, nextWeek);

        var fullDate = getFullDateObj(todayObj, nextWeekObj);
        this.setWeek(today);
        eventHandler.fire.call(this, this.options.onToday, evt, fullDate);
        // eventHandler.fire.call(this, this.options.onChange, evt, fullDate);
    }

    function setCurrentWeek(week) {
        this.currentWeek = new Date(week);
    }

    function setWeek(startDate, endDate) {
        deselectAll();
        var htmlDays = document.getElementsByClassName("day");
        for (var d = startDate, i = 0, n = endDate; d < endDate; d.setDate(d.getDate() + 1), i++) {
            var date = new Date(d);
            var dateObj = getDateObj.call(this, d);

            var trDay = htmlDays[i];
            trDay.date = dateObj;
            trDay.firstChild.innerHTML = this.options.valueFormat(dateObj.day + DateUtility.nth(dateObj.day)
                + ", " + dateObj.dayName, dateObj);
            selectToday.call(this, trDay, date);

            var label = document.querySelectorAll(".month-year.label")[0];
            label.innerHTML = dateObj.monthName + ", " + dateObj.year;
        }
    }

    function deselectAll() {
        var activeElements = document.querySelectorAll(".active");
        activeElements.forEach(function (el) {
            DomHelper.removeClass(el, "active");
        })
    }

    function selectToday(trDay, date) {
        //Highlight today's date
        if (DateUtility.compare(date, this.date) == 0) {
            if (!this.options.todayClass) DomHelper.addClass(trDay, "today");
            DomHelper.addClass(trDay, this.options.todayClass);
        }
        else {
            //Default and user-defined classes
            var todayClasses = "today " + this.options.todayClass;
            DomHelper.removeClass(trDay, todayClasses);
        }
    }

    function getDateObj(week) {
        var locale = this.options.locale;
        var date = new Date(week);
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        var currentWeek = DateUtility.getWeekNumber(date);
        var dateObj = {
            date: date,
            day: day,
            week: currentWeek,
            month: month,
            year: year,
            dayName: date.toLocaleString(locale, {weekday: "long"}),
            dayNameShort: date.toLocaleString(locale, {weekday: "short"}),
            monthName: date.toLocaleString(locale, {month: "long"}),
            monthNameShort: date.toLocaleString(locale, {month: "short"}),
        }
        return dateObj;
    }

    function getFullDateObj(start, end, prev) {
        return {startDate: start, endDate: end, previousDate: prev};
    }

    function generateHtml() {
        var container, header, label, table, navbarBody;
        container = document.createElement("div");
        container.className = "calendar-navbar";

        header = document.createElement("div");
        header.className = "calendar-navbar-header";


        label = document.createElement("span");
        label.className = "month-year label";
        label.innerHTML = "";
        label.addEventListener("click", onToday.bind(this), false);

        this.leftButton = document.createElement("span");
        this.leftButton.className = "left no-selection";
        if (!this.options.buttonClass) DomHelper.addClass(this.leftButton, "button");
        DomHelper.addClass(this.leftButton, this.options.buttonClass);
        this.leftButton.innerHTML = this.options.leftButtonText;

        this.rightButton = document.createElement("span");
        this.rightButton.className = "right no-selection";
        if (!this.options.buttonClass) DomHelper.addClass(this.rightButton, "button");
        DomHelper.addClass(this.rightButton, this.options.buttonClass);
        this.rightButton.innerHTML = this.options.rightButtonText;


        header.appendChild(this.leftButton);
        header.appendChild(label);
        header.appendChild(this.rightButton);

        container.appendChild(header);

        table = document.createElement("table");
        table.className = "days";
        var tbody = document.createElement("tbody");
        var tr = document.createElement("tr");
        for (var i = 0, n = 7; i < n; i++) {
            var dayNo = i + 1;
            var td = document.createElement("td");
            var span = document.createElement("span");
            span.className = "day-text";
            td.className = "day day-" + dayNo;
            td.appendChild(span);
            tr.appendChild(td);
            td.addEventListener("click", onSelect.bind(this), true);
        }
        tbody.appendChild(tr);
        table.appendChild(tbody);

        navbarBody = document.createElement("div");
        navbarBody.className = "calendar-navbar-body";
        navbarBody.appendChild(table);
        container.appendChild(navbarBody);
        this.$el.appendChild(container);
    }

    function extend() {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    }

    function initEvents() {
        if (this.rightButton) {
            this.rightButton.addEventListener("click", onNext.bind(this));
        }
        if (this.leftButton) {
            this.leftButton.addEventListener("click", onPrevious.bind(this));
        }
        if (this.todayButton) {
            this.todayButton.addEventListener("click", onToday.bind(this));
        }
        var startDate = getDateObj.call(this, this.currentWeek);
        var fullDate = getFullDateObj(startDate, this.getByDayNumber(6));
        eventHandler.fire.call(this, this.options.onInitComplete, fullDate);
    }


    //#endregion
})();